#include "Node.cpp"
#include <iostream>
#include <stdexcept>
template<typename T>
class MyStack{
    private:
        Node<T>* head=nullptr;
        int size=0;
    public :
        MyStack(){}
        void push(T value){
            head=new Node<T>(value,head);
            size++;
        }
        T pop(){
            if(head==nullptr){
                throw std::out_of_range("Empty");
            }
            T tmp=head->getValue();
            head=head->getNext();
            size--;
            return tmp;
        }
        T peek(){
            if(head==nullptr)
                throw std::out_of_range("Empty");
            return head->getValue();
        }        
        bool isEmpty(){
            return size==0;
        }
        int getSize(){
            return size;
        }

};