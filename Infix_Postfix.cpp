#include <string>
#include <iostream>
#include <stdio.h>
#include "MyStack.cpp"
std::string getPostfix(std::string infix){
    std::string result="";
    MyStack<char> myStack;
    for(int i=0;i<infix.length();i++){
        char current=infix.at(i);
        if(current=='+'||current=='-'){
            try{
                char tmp=myStack.peek();
                while(tmp=='+'||tmp=='-'||tmp=='*'||tmp=='/'){
                    result+=myStack.pop();
                    tmp=myStack.peek();
                }
                myStack.push(current);
            }
            catch(std::out_of_range){
                myStack.push(current);
            }
        }
        else if(current=='*'||current=='/'){
            try{
                char tmp=myStack.peek();
                if(tmp=='*'||tmp=='/')
                    result+=myStack.pop();
                myStack.push(current);
            }
            catch(std::out_of_range){
                myStack.push(current);
            } 
        }
        else if(current=='(')
            myStack.push(current);
        else if(current==')'){
            while(myStack.peek()!='('){
                result+=myStack.pop();
            }
            myStack.pop();
        }
        else    
            result+=current;
    }
    while(myStack.isEmpty()==false){
        result+=myStack.pop();
    }
    return result;
}
std::string getInfix(std::string postfix){
    MyStack<std::string> myStack;
    for (int i=0;i<postfix.size();i++){
        char current=postfix.at(i);
        if(current=='+'||current=='-'||current=='*'||current=='/'){
            std::string value1=myStack.pop(),value2=myStack.pop();
            value2=value2+current+value1;
            myStack.push(value2);
        }
        else{
            std::string convert="";
            convert+=current;
            myStack.push(convert);
        }
    }
    return myStack.pop();
}
int getIntegerInfix(std::string postfix){
    MyStack<int> myStack;
    for (int i=0;i<postfix.size();i++){
        char current=postfix.at(i);
        if(current=='+'){
            int value1=myStack.pop(),value2=myStack.pop();
            myStack.push(value2+value1);
        }
        else if(current=='-'){
            int value1=myStack.pop(),value2=myStack.pop();
            myStack.push(value2-value1);
        }
        else if(current=='*'){
            int value1=myStack.pop(),value2=myStack.pop();
            myStack.push(value2*value1);
        }
        else if(current=='/'){
            int value1=myStack.pop(),value2=myStack.pop();
            myStack.push(value2/value1);
        }
        else if(current==' '){
            std::cout<<"check";
            continue;
        }
        else{
            int tmp;
            char convert[1];
            sprintf(convert,"%c",current);
            sscanf(convert,"%d",&tmp);
            myStack.push(tmp);
        }
    }
    return myStack.pop();
}
int main(){
    std::string input;
    std::cin>>input;
    std::cout<<"Integer Infix:"<<getIntegerInfix(input);
    return 0;
}