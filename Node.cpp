#include <iostream>
#include <string>
template<typename T>
class Node{
    private:
        T value;
        Node<T>* next=nullptr;
    public:
        Node(T value,Node<T>* next){
            this->value=value;
            this->next=next;
        }
        T getValue(){
            return value;
        }
        Node<T>* getNext(){
            return next;
        }
        void setNext(Node<T>* newValue){
            this->next=newValue;
        }
        void setValue(T value){
            this->value=value;
        }
};